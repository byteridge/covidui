export interface CountryWise {
  id: string;
  country: string;
  active: number;
  confirmed: number;
  recovered: number;

  deaths: number;
  latitude: number;
  longitude: number;
  last_update: Date;
}
