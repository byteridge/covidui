import * as React from "react";
import "./App.css";
import { Provider } from "react-redux";
import store from "./store";
import Container from "./components/Container";
import Covid19table from "./components/CovidTable";

function App() {
  return (
    <Provider store={store}>
      <Container />
      {/* <Covid19table /> */}
    </Provider>
  );
}

export default App;
