import { State } from "../store/reducer";
import { getTitle } from "../store/selectors";
import { connect } from "react-redux";
import * as React from "react";
import { covidDataCountryWise } from "../store/api";
import { bindActionCreators } from "redux";
import { Layout, Menu, Breadcrumb } from "antd";
import Covid19table from "./CovidTable";
import CountryMap from "./CountryMap";

interface ConnectProps {
  title: string;
}

interface DispatchProps {
  covidDataCountryWise: () => void;
}

type Props = ConnectProps & DispatchProps;

const ContainerImpl: React.FunctionComponent<Props> = (props) => {
  React.useEffect(() => {
    props.covidDataCountryWise();
  });

  const { Header, Content } = Layout;

  return (
    <Layout className="layout">
      <Header>
        <h3 className="header-text">{props.title}</h3>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">List</Menu.Item>
          <Menu.Item key="2">Map</Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <div className="site-layout-content">
          {/* <CountryMap /> */}
          <Covid19table />
        </div>
      </Content>
      {/* <Footer style={{ textAlign: "center" }}>
        Ant Design ©2018 Created by Ant UED
      </Footer> */}
    </Layout>
  );
};

function mapStateToProps(state: State): ConnectProps {
  return {
    title: getTitle(state),
  };
}

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      covidDataCountryWise,
    },
    dispatch
  );

const Container = connect(mapStateToProps, mapDispatchToProps)(ContainerImpl);

export default Container;
